# https://www.django-rest-framework.org/api-guide/viewsets/
# https://www.youtube.com/watch?v=dCbfOZurCQk
# https://stackoverflow.com/questions/58558989/what-does-djangos-property-do

from django.shortcuts import render
from rest_framework import viewsets
from api.models import Post
from api.serializers import PostSerializer
from rest_framework.decorators import action
from rest_framework.response import Response


# Create your views here.
class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

    # def create_post(self, request):
    #     data = request.data
    #     posts = Post.objects.create(
    #         post_type=data[True],
    #         content=data['content']
    #     )
    #     return Response({'id': posts.id})

    @action(detail=False)
    def boast(self, request):
        posts = Post.objects.filter(post_type=True).order_by('-created_at')
        serializer = self.get_serializer(posts, many=True)
        return Response(serializer.data)

    @action(detail=False)
    def roast(self, request):
        posts = Post.objects.filter(post_type=False).order_by('-created_at')
        serializer = self.get_serializer(posts, many=True)
        return Response(serializer.data)

    @action(detail=True, methods=['post'])
    def like(self, request, pk=None):
        posts = Post.objects.get(pk=pk)
        posts.likes += 1
        posts.save()
        return Response({'id': posts.id, 'likes': posts.likes})

    @action(detail=True, methods=['post'])
    def dislike(self, request, pk=None):
        posts = Post.objects.get(pk=pk)
        posts.dislikes += 1
        posts.save()
        return Response({'id': posts.id, 'dislikes': posts.dislikes})

    @action(detail=False)
    def highest_votes(self, request, pk=None):
        all_votes = Post.objects.all()
        sorted_votes = list(all_votes)
        sorted_votes = sorted(sorted_votes, key=lambda a: a.total, reverse=True)
        serializer = self.get_serializer(sorted_votes, many=True)
        return Response(serializer.data)

    @action(detail=False)
    def vote_score(self, request, pk=None):
        all_votes = Post.objects.all()
        sorted_votes = list(all_votes)
        sorted_votes = sorted(sorted_votes, key=lambda a: a.vote_score, reverse=True)
        serializer = self.get_serializer(sorted_votes, many=True)
        return Response(serializer.data)

    @action(detail=True, methods=['delete'])
    def delete_post(self, request, pk=id):
        gone = Post.objects.get(secret_key=pk)
        gone.delete()
        return Response({'status': 'deleted'})
