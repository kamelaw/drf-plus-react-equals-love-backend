# grabbed ghostpost model
# https://stackoverflow.com/questions/25943850/django-package-to-generate-random-alphanumeric-string/25949808

from django.db import models
from django.utils import timezone
from django.utils.crypto import get_random_string


# Create your models here.
# one model that represents both boasts and roasts
class Post(models.Model):
    Post_Choices = (
        (True, 'Boast'),
        (False, 'Roast')
    )
    post_type = models.BooleanField(choices=Post_Choices, default=True)  # null=True, default=True
    text = models.CharField(max_length=280)
    # at the beginning when we start they'll be at 0
    likes = models.IntegerField(default=0)
    dislikes = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)
    secret_key = models.CharField(max_length=6, blank=True, editable=False,
                                  default=get_random_string(6).lower())

    # And then just call the method in your template
    @property
    def total(self):
        return self.likes + self.dislikes

    @property
    def vote_score(self):
        return self.likes - self.dislikes

    # makes it more readable for admin panel
    def __str__(self):
        return self.text
