from rest_framework.serializers import HyperlinkedModelSerializer
from api.models import Post


class PostSerializer(HyperlinkedModelSerializer):
    class Meta:
        model = Post
        fields = [
            'post_type',
            'id',
            'text',
            'likes',
            'dislikes',
            'created_at',
            'updated_at',
            'total',
            'vote_score'

        ]
